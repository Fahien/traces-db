ci-fairy minio login $CI_JOB_JWT --endpoint-url http://192.168.178.24:9000
if [[ $CI_PROJECT_NAMESPACE == "Fahien" ]]; then
    UPLOAD_URL="minio://192.168.178.24:9000/mesa-tracie-public"
else
    UPLOAD_URL="minio://192.168.178.24:9000/artifacts/$CI_PROJECT_PATH/$CI_PIPELINE_ID/traces"
fi
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for f in $(git ls-files -- '*.rdc' '*.trace' '*.gfxr' '*.trace-dxgi'); do
    echo "Uploading $UPLOAD_URL/$f"
    ci-fairy minio cp "$f" "$UPLOAD_URL/$f"
done
IFS=$SAVEIFS
